# alpine-pureftpd

#### [alpine-x64-pureftpd](https://hub.docker.com/r/forumi0721alpinex64build/alpine-x64-pureftpd/)
[![](https://images.microbadger.com/badges/version/forumi0721alpinex64build/alpine-x64-pureftpd.svg)](https://microbadger.com/images/forumi0721alpinex64build/alpine-x64-pureftpd "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpinex64build/alpine-x64-pureftpd.svg)](https://microbadger.com/images/forumi0721alpinex64build/alpine-x64-pureftpd "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinex64build/alpine-x64-pureftpd.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinex64build/alpine-x64-pureftpd/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinex64build/alpine-x64-pureftpd.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinex64build/alpine-x64-pureftpd/)
#### [alpine-aarch64-pureftpd](https://hub.docker.com/r/forumi0721alpineaarch64build/alpine-aarch64-pureftpd/)
[![](https://images.microbadger.com/badges/version/forumi0721alpineaarch64build/alpine-aarch64-pureftpd.svg)](https://microbadger.com/images/forumi0721alpineaarch64build/alpine-aarch64-pureftpd "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpineaarch64build/alpine-aarch64-pureftpd.svg)](https://microbadger.com/images/forumi0721alpineaarch64build/alpine-aarch64-pureftpd "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpineaarch64build/alpine-aarch64-pureftpd.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpineaarch64build/alpine-aarch64-pureftpd/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpineaarch64build/alpine-aarch64-pureftpd.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpineaarch64build/alpine-aarch64-pureftpd/)
#### [alpine-armhf-pureftpd](https://hub.docker.com/r/forumi0721alpinearmhfbuild/alpine-armhf-pureftpd/)
[![](https://images.microbadger.com/badges/version/forumi0721alpinearmhfbuild/alpine-armhf-pureftpd.svg)](https://microbadger.com/images/forumi0721alpinearmhfbuild/alpine-armhf-pureftpd "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpinearmhfbuild/alpine-armhf-pureftpd.svg)](https://microbadger.com/images/forumi0721alpinearmhfbuild/alpine-armhf-pureftpd "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinearmhfbuild/alpine-armhf-pureftpd.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinearmhfbuild/alpine-armhf-pureftpd/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinearmhfbuild/alpine-armhf-pureftpd.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinearmhfbuild/alpine-armhf-pureftpd/)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64,armhf
* Appplication : [Pure-FTPd](https://www.pureftpd.org/)
    - Pure-FTPd is a free (BSD), secure, production-quality and standard-conformant FTP server.
* Base Image
    - [forumi0721/alpine-x64-base](https://hub.docker.com/r/forumi0721/alpine-x64-base/)
    - [forumi0721/alpine-aarch64-base](https://hub.docker.com/r/forumi0721/alpine-aarch64-base/)
    - [forumi0721/alpine-armhf-base](https://hub.docker.com/r/forumi0721/alpine-armhf-base/)



----------------------------------------
#### Run

* x64
```sh
docker run -d \
           -p 20:20/tcp \
           -p 21:21/tcp \
           -p 60000-60099:60000-60099/tcp \
           -v /data:/data \
           -e USER_NAME=<username> \
           -e USER_PASSWD=<passwd> \
           -e FTP_PUBLIC_HOST=<hostname> \
           -e FTP_SHARE=/data \
           forumi0721alpinex64build/alpine-x64-pureftpd:latest
```

* aarch64
```sh
docker run -d \
           -p 20:20/tcp \
           -p 21:21/tcp \
           -p 60000-60099:60000-60099/tcp \
           -v /data:/data \
           -e USER_NAME=<username> \
           -e USER_PASSWD=<passwd> \
           -e FTP_PUBLIC_HOST=<hostname> \
           -e FTP_SHARE=/data \
           forumi0721alpineaarch64build/alpine-aarch64-pureftpd:latest
```

* armhf
```sh
docker run -d \
           -p 20:20/tcp \
           -p 21:21/tcp \
           -p 60000-60099:60000-60099/tcp \
           -v /data:/data \
           -e USER_NAME=<username> \
           -e USER_PASSWD=<passwd> \
           -e FTP_PUBLIC_HOST=<hostname> \
           -e FTP_SHARE=/data \
           forumi0721alpinearmhfbuild/alpine-armhf-pureftpd:latest
```



----------------------------------------
#### Usage

* URL : [ftp://localhost:21/](ftp://localhost:21/)
    - Default username/password : forumi0721/passwd
    - If you want to connect dockerlized pureftpd, you need positve mode to connect or set `--net=host` to docker option.



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| 20/tcp             | FTP data port                                    |
| 21/tcp             | FTP port                                         |
| 60000-60099/tcp    | FTP passive mode ports                           |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| /data              | FTP share directory                              |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| USER_NAME          | Login username (default : forumi0721)            |
| USER_PASSWD        | Login password (default : passwd)                |
| USER_EPASSWD       | Login password (base64)                          |
| FTP_PUBLIC_HOST    | FTP public hostname (default : localhost)        |
| FTP_SHARE          | FTP share directory (default : /data)            |
| FTP_PASV_MIN_PORT  | FTP passive min port (default : 60000)           |
| FTP_PASV_MAX_PORT  | FTP passive max port (default : 60099)           |



----------------------------------------
* [forumi0721alpinex64build/alpine-x64-pureftpd](https://hub.docker.com/r/forumi0721alpinex64build/alpine-x64-pureftpd/)
* [forumi0721alpineaarch64build/alpine-aarch64-pureftpd](https://hub.docker.com/r/forumi0721alpineaarch64build/alpine-aarch64-pureftpd/)
* [forumi0721alpinearmhfbuild/alpine-armhf-pureftpd](https://hub.docker.com/r/forumi0721alpinearmhfbuild/alpine-armhf-pureftpd/)

